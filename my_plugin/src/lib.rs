// #![crate_type = "dylib"]
#![feature(plugin_registrar, rustc_private)]

extern crate rustc;
extern crate syntax;

use rustc::plugin::Registry;

use syntax::ast;
use syntax::codemap;
use syntax::ext::base::{DummyResult, ExtCtxt, MacEager, MacResult};

#[plugin_registrar]
#[doc(hidden)]
pub fn plugin_registrar(registry: &mut Registry) {
    registry.register_macro("my_macro", expand_mymacro);
}

fn expand_mymacro(cx: &mut ExtCtxt, sp: codemap::Span, tts: &[ast::TokenTree])
          -> Box<MacResult+'static> {
    let mut parser = cx.new_parser_from_tts(tts);
    let expr = parser.parse_expr();
    println!("In my plugin.  context: {:?}\n\nspan: {:?}\n\ntoken_tree: {:?}", expr, sp, tts);
    DummyResult::expr(sp)
}
