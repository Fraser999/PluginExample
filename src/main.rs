#![feature(plugin)]
#![plugin(my_plugin)]

fn main() {
    let result = my_macro!(Test);
    println!("{}", result);
}
